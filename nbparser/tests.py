import decimal
import unittest
import datetime
from nbparser import (get_nbp_currency_xml_url, parse_currency_exchange_rate,
                      CurrencyXmlNotFound, CurrencyExchangeRateNotFound)
import textwrap


class TestClient(unittest.TestCase):

    def setUp(self):
        super(TestClient, self).setUp()

    def test_xml_filename_in_dir_txt(self):
        day = datetime.date(year=2013, month=12, day=11)
        xml_file = get_nbp_currency_xml_url(day)
        self.assertTrue(xml_file is not None)

    def test_xml_filename_not_in_dir_txt(self):
        day = datetime.date(year=1901, month=1, day=1)
        with self.assertRaises(CurrencyXmlNotFound):
            get_nbp_currency_xml_url(day)

    currency_xml = textwrap.dedent("""\
        <?xml version="1.0" encoding="ISO-8859-2"?>
        <!DOCTYPE tabela_kursow SYSTEM "abch.dtd">
        <tabela_kursow typ="A">
           <numer_tabeli>1/A/NBP/2002</numer_tabeli>
           <data_publikacji>2002-01-02</data_publikacji>
           <pozycja>
              <nazwa_kraju>Australia</nazwa_kraju>
              <symbol_waluty>781</symbol_waluty>
              <przelicznik>1</przelicznik>
              <kod_waluty>AUD</kod_waluty>
              <kurs_sredni>2,0227</kurs_sredni>
           </pozycja>
           <pozycja>
              <nazwa_kraju>Austria</nazwa_kraju>
              <symbol_waluty>786</symbol_waluty>
              <przelicznik>1</przelicznik>
              <kod_waluty>ATS</kod_waluty>
              <kurs_sredni>0,2580</kurs_sredni>
           </pozycja>
        </tabela_kursow>""")

    def test_parsing_currency_rate(self):
        rate = parse_currency_exchange_rate(self.currency_xml, 'AUD')
        self.assertEqual(rate, decimal.Decimal('2.0227'))

    def test_parsing_non_existing_currency_rate_raises_exception(self):
        self.assertRaises(CurrencyExchangeRateNotFound,
                          lambda: parse_currency_exchange_rate(self.currency_xml, 'PLN'))



if __name__ == '__main__':
    unittest.main()