import datetime
import dateutil.easter
import decimal
import re
import urllib2
from bs4 import BeautifulSoup

__all__ = ['get_currency_exchange_rate', 'CurrencyXmlNotFound',
           'CurrencyExchangeRateNotFound']

nbb_currency_endpoint = 'http://www.nbp.pl/kursy/xml/'
nbp_currency_dir_endpoint = nbb_currency_endpoint + 'dir.txt'
nbp_currency_list_endpoint = nbb_currency_endpoint + '%s.xml'


class CurrencyXmlNotFound(Exception):

    pass


class CurrencyExchangeRateNotFound(Exception):

    pass


def calc_holidays(year):
    holidays = [
        datetime.date(year, 1, 1),  #: nowy rok
        datetime.date(year, 1, 6),  #: trzech kroli (ustawowo wolne od 2013r)
        datetime.date(year, 5, 1),  #: 1. maja
        datetime.date(year, 5, 3),  #: 3. maja
        datetime.date(year, 8, 15),  #: Wniebowziecie NMP
        datetime.date(year, 11, 1),  #: Wszystkich Swietych
        datetime.date(year, 11, 11),  #: Narodowe Swieto Niepodleglosci
        datetime.date(year, 12, 25),  #: Boze Narodzenie
        datetime.date(year, 12, 26),  #: Boze Narodzenie
    ]
    easter = dateutil.easter.easter(year)
    holidays += [
        easter,  #: Wielkanoc
        easter + datetime.timedelta(days=1),  #: Wielkanoc
        easter + datetime.timedelta(days=49),  #: Zielone Swiatki
        easter + datetime.timedelta(days=60),  #: Boze Cialo
    ]
    return holidays


def get_last_working_day(date):
    if isinstance(date, datetime.datetime):
        date = date.date()
    last = date + datetime.timedelta(days=-1)
    holidays = calc_holidays(last.year)
    while last in holidays or last.weekday() > 4:
        last += datetime.timedelta(days=-1)
    return last


def get_nbp_currency_xml_url(date):
    last_working_day = get_last_working_day(date)
    file_pattern = re.compile('^a[0-9]{3}z%(date)s$' % {
        'date': last_working_day.strftime('%y%m%d')
    })
    sock = urllib2.urlopen(nbp_currency_dir_endpoint)
    for xml_filename in sock.read().splitlines():
        if re.match(file_pattern, xml_filename):
            return nbp_currency_list_endpoint % xml_filename
    raise CurrencyXmlNotFound('Could not find nbparser XML file '
                              'with date %s' % date)


def parse_currency_exchange_rate(currency_xml, currency):
    soup = BeautifulSoup(currency_xml, 'xml')
    currencies = soup.find_all('pozycja')  #: really NBP?
    for currency_item in currencies:
        currency_code = currency_item.find('kod_waluty').string
        if currency_code == currency:
            exchange_rate = currency_item.find('kurs_sredni').string
            # snippet ripped from babel.parse_decimal in
            # context of polish locale
            group_separator = '.'
            decimal_separator = ','
            return decimal.Decimal(exchange_rate.replace(group_separator, '')
                                                .replace(decimal_separator, '.'))
    raise CurrencyExchangeRateNotFound()


def get_currency_exchange_rate(date, currency):
    currency_xml_url = get_nbp_currency_xml_url(date)
    sock = urllib2.urlopen(currency_xml_url)
    currency_xml = sock.read()
    return parse_currency_exchange_rate(currency_xml, currency)
