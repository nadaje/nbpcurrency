from distutils.core import setup

setup(
    name='nbpcurrency',
    version='dev',
    packages=['nbparser'],
    url='',
    license='',
    author='ra2er',
    author_email='sylwester.kulpa@gmail.com',
    description='',
    install_requires=['beautifulsoup4', 'dateutils', 'mock', 'lxml']
)
